package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import javax.lang.model.util.ElementScanner14;

import java.util.Random;
public class RockPaperScissors {

	public static void main(String[] args) {
    	/*
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }


    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    public void run() {
        System.out.println("Let's play round " + roundCounter);
        String inp = readInput();
        if (inp.equals("Wrong")){readInput();}
        else {
            roundCounter++;
            System.out.println("Score: human " + humanScore + " computer " + computerScore);
            System.out.println("Do you wish to continue playing? (y/n)?");
            char fin = sc.next().charAt(0);
            if (fin == 'y'){
                run();
            } else{
                System.out.println("Bye bye :)");

            }

            }



        }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput() {
            System.out.println("Your choice (Rock/Paper/Scissors)?");
            String userInput = sc.next();
            String computer_choice = tester();
            if (rpsChoices.contains(userInput)) {
                winner(computer_choice, userInput);
                return "Fin";
           } else {
            System.out.println("I do not understand " + userInput  + ". Could you try again?");
            return "Wrong";
        }
    }
    public String tester(){
        Random rand = new Random();
        int random_int = rand.nextInt(3);
        String choice = " ";
        if (random_int == 0 ) {
            choice = "rock";
        } else if (random_int == 1) {
            choice = "paper";
        } else
            choice = "scissors";
        return choice;
    }
    public void winner(String comp, String player){
        if (player.equals(comp)) {
            System.out.println("Human chose " + player + ", computer chose " + comp + ". it's a tie!");
        } else if (comp.equals("scissors") & (player.equals("paper")) ){
            System.out.println("Human chose " + player + ", computer chose " + comp + ". Computer wins!");
            computerScore ++;
        } else if (comp.equals("paper") & (player.equals("rock")) ){
            System.out.println("Human chose " + player + ", computer chose " + comp + ". Computer wins!");
            computerScore ++;
        } else if (comp.equals("rock") & (player.equals("sissors")) ){
            System.out.println("Human chose " + player + ", computer chose " + comp + ". Computer wins!");
            computerScore ++;
        } else {
            System.out.println("Human chose " + player + ", computer chose " + comp + ". Human wins!");
            humanScore ++;
        }

    }
}
